﻿using System;
using System.Diagnostics;
using System.Timers;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Reflection;

namespace Reflections
{
  class Program
  {
    static void Main(string[] args)
    {
      Data data = Data.Get();
      var timer1 = new Stopwatch();
      timer1.Start();
      for (int i = 0; i < 1000; i++)
      {
        SerializatingFromReflection(data);
      }
      timer1.Stop();

      var timer2 = new Stopwatch();
      timer2.Start();
      for (int i = 0; i < 1000; i++)
      {
        Console.WriteLine(SerializatingFromReflection(data));
      }
      timer2.Stop();
      //4.Вывести в консоль полученную строку и разницу времен
      Console.WriteLine($"only serialing = {timer1.ElapsedMilliseconds}");
      //6.Замерить время еще раз и вывести в консоль сколько потребовалось времени на вывод текста в консоль
      Console.WriteLine($"Serializing and writing - {timer2.ElapsedMilliseconds}");

      var timerJson = new Stopwatch();
      timerJson.Start();
      for (int i = 0; i < 1000; i++)
      {
        Console.WriteLine(JsonConvert.SerializeObject(data));
      }
      timerJson.Stop();
      //7. Провести сериализацию с помощью каких-нибудь стандартных механизмов (например в JSON)
      Console.WriteLine($"Serializing and writing Json - {timer2.ElapsedMilliseconds}");

      //9.Написать десериализацию / загрузку данных из строки(ini / csv - файла) в экземпляр любого класса
      //Сериализуем через csv
      var timerCSVSerialization = new Stopwatch();
      using (var writer = new StreamWriter("file.csv"))
      {
        timerCSVSerialization.Start();
        for (int i = 0; i < 1000; i++)
        {
          writer.WriteLine(string.Join(";", data.GetType().GetFields().Select(x => x.GetValue(data)).ToList()));
        }
        timerCSVSerialization.Stop();
      }
      Console.WriteLine($"Serialization and writing in CSV - {timerCSVSerialization.ElapsedMilliseconds}");
      //Десериализуем через csv
      //Data data2 = null;
      var timerCSVDeserialization = new Stopwatch();
      timerCSVDeserialization.Start();
      using (var reader = new StreamReader("file.csv"))
      {
        foreach (string line in reader.ReadToEnd().Split("\r\n").ToList())
        {
          // инициализируем пустой экземпляр
          data = new Data();
          string[] arrline = line.Split(';');
          int cursor = 0;
          if (arrline.Length == data.GetType().GetFields().Count())
          {
            foreach (FieldInfo field in data.GetType().GetFields())
            {
              if (field.FieldType == typeof(System.String))
                data.GetType().GetField(field.Name).SetValue(data, arrline[cursor].ToString());
              if (field.FieldType == typeof(Int32))
                data.GetType().GetField(field.Name).SetValue(data, Convert.ToInt32(arrline[cursor]));
              //.... и так по всем типам, чтобы корректно обработать конвертацию по всем типам
              cursor++;
            }
            //Console.WriteLine(string.Join(";", data.GetType().GetFields().Select(x => x.GetValue(data)).ToList()));
          }
        }
      }
      timerCSVDeserialization.Stop();
      Console.WriteLine($"Deerialization and reading of CSV - {timerCSVDeserialization.ElapsedMilliseconds}");
      //SerializatingFromReflection(data2);
      //Console.WriteLine($"Deserialization and writing CSV - {timerCSVSerialization.ElapsedMilliseconds}");
    }

    private static string SerializatingFromReflection(object d)
    {
      string methods = String.Join("\n", d.GetType().GetMethods().Select(x => x.Name.ToString()).ToList());
      string fields = String.Join("\n", d.GetType().GetFields().Select(x => x.Name.ToString() + " - " + x.GetValue(d).ToString()).ToList());
      return methods + "\n" + fields;
    }
  }
}
